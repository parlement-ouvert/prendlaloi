#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Récupère sur Eliasse des amendements et de conversion en pages Markdown"""


import argparse
import json
import os
import sys

from prendlaloi import (
    DocumentCompleter,
    MarkdownWriter,
    )


amendements_by_auteur_markdown_template = '''\
<!-- TITLE: Amendements par groupe et par auteur -->

% if amendements_by_auteur_by_groupe:
    % for groupe, amendements_by_auteur in sorted(amendements_by_auteur_by_groupe.items()):
£ ${groupe}

        % for nom_prenom, amendements in sorted(amendements_by_auteur.items()):
££ ${nom_prenom[0]}, ${nom_prenom[1]}

${articles_tree(amendements, 0)}

        % endfor

    % endfor
% endif

% if amendements_from_gouvernement:
£ Gouvernement

${articles_tree(amendements_from_gouvernement, 0)}

% endif
'''


article_markdown_template = '''\
<!-- TITLE: ${numero} -->
% if sortEnSeance:
<!-- SUBTITLE: ${sortEnSeance} -->
% endif

% if auteur['estGouvernement']:
Amendement du Gouvernement
% else:
**Amendement de ${auteur['civilite']} ${auteur['prenom']} ${auteur['nom']}${auteur['qualite'] or ''}${' ' + cosignatairesMentionLibre['titre'] if cosignatairesMentionLibre else ''}**
% endif

% if place:
${place}
% endif

# Titre

% if paragraphes:
${paragraphes}
% endif

% if exposeSommaire:
# Exposé sommaire

${exposeSommaire}
% endif
'''


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

* [Index des amendements par auteur](${url_path}/index-amendements-par-auteur)
${sections_tree(sections, 0)}
'''


app_name = os.path.splitext(os.path.basename(__file__))[0]
bibard = '490'
legislature = 15
organe = 'CION_LOIS'
organes = [
    'AN',  # Séance, Hémicycle,
    'CION-CEDU',  # Commission Affaires culturelles et éducation
    'CION_LOIS',  # Commission Lois
    # TODO: Complete.
    ]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'source_dir',
        help='path of source directory containing JSON files',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    document_slug = 'amendements-1-assemblee-commission'
    amendements_json_dir = os.path.join(args.source_dir, document_slug)

    with open(os.path.join(amendements_json_dir, 'discussion.json'), 'r') as json_file:
        data = json.load(json_file)
    assert sorted(data.keys()) == ["amdtsParOrdreDeDiscussion" ], sorted(data.keys())
    amdtsParOrdreDeDiscussion = data['amdtsParOrdreDeDiscussion']
    assert sorted(amdtsParOrdreDeDiscussion.keys()) == [
        'amendements',
        'amendementsByNum',
        'bibard',
        'bibardSuffixe',
        'divisions',
        'legislature',
        'organe',
        'titre',
        'type',
        ], sorted(amdtsParOrdreDeDiscussion.keys())

    amendements_by_auteur_by_groupe = {}
    amendements_from_gouvernement = []
    articles = None
    placeReference = None
    section = None
    sections = []
    for resume_amendement in amdtsParOrdreDeDiscussion['amendements']:
        assert sorted(resume_amendement.keys()) == [
            'alineaLabel',
            'auteurGroupe',
            'auteurLabel',
            'discussionCommune',
            'discussionCommuneAmdtPositon',
            'discussionCommuneSsAmdtPositon',
            'discussionIdentique',
            'discussionIdentiqueAmdtPositon',
            'discussionIdentiqueSsAmdtPositon',
            'missionLabel',
            'numero',
            'parentNumero',
            'place',
            'position',
            'sort',
            ], sorted(resume_amendement.keys())

        json_filename = 'amendement-{}.json'.format(resume_amendement['numero'])
        with open(os.path.join(amendements_json_dir, json_filename), 'r') as json_file:
            data = json.load(json_file)
        assert sorted(data.keys()) == ["amendements" ], sorted(data.keys())
        amendements = data['amendements']
        assert isinstance(amendements, list)
        assert len(amendements) == 1
        amendement = amendements[0]
        assert sorted(amendement.keys()) == [
            'accordGouvernement',
            'alinea',
            'ancreDivisionTexteVise',
            'auteur',
            'auteurParent',
            'bibard',
            'bibardSuffixe',
            'cosignataires',
            'cosignatairesMentionLibre',
            'dispositif',
            'division',
            'etat',
            'exposeSommaire',
            'legislature',
            'listeDesSignataires',
            'numero',
            'numeroLong',
            'numeroParent',
            'numeroReference',
            'organeAbrv',
            'place',
            'placeReference',
            'position',
            'sortEnSeance',
            'texte',
            'urlPDF',
            ], sorted(amendement.keys())
        accordGouvernement = amendement['accordGouvernement']
        assert isinstance(accordGouvernement, dict)
        assert sorted(accordGouvernement.keys()) == ["accord", "libelle" ], sorted(accordGouvernement.keys())
        assert accordGouvernement['accord'] is None, amendement
        assert accordGouvernement['libelle'] is None, amendement

        assert amendement['etat'] in ('AC', 'DI'), amendement['etat']

        if amendement['placeReference'] is None:
            # Correct the bug in source data.
            amendement['placeReference'] = placeReference

        assert amendement['sortEnSeance'] in ('', 'Adopté', 'Non soutenu', 'Rejeté', 'Retiré', 'Tombé'), amendement['sortEnSeance']

        if section is None or amendement['placeReference'] != section['numero']:
            assert amendement['placeReference'], json.dumps(data, ensure_ascii=False, indent=2, sort_keys=True)
            articles = []
            placeReference = amendement['placeReference']
            section = dict(
                articles=articles,
                numero=placeReference,
                )
            sections.append(section)
        auteur = amendement['auteur']
        auteur["estGouvernement"] = bool(int(auteur["estGouvernement"]))
        auteur["estRapporteur"] = bool(int(auteur["estRapporteur"]))
        article = dict(
            auteur=auteur,
            cosignatairesMentionLibre=amendement['cosignatairesMentionLibre'],
            exposeSommaire=amendement['exposeSommaire'],
            numero='Amendement n° {}'.format(amendement['numero']),
            numeroLong=amendement['numeroLong'],
            paragraphes=amendement['dispositif'],
            place=amendement['place'],
            sortEnSeance=amendement['sortEnSeance'],
            titre=amendement['sortEnSeance'] or None,
            )
        articles.append(article)
        if auteur['estGouvernement']:
            amendements_from_gouvernement.append(article)
        else:
            groupe = resume_amendement['auteurGroupe']
            amendements_by_auteur_by_groupe.setdefault(groupe, {}).setdefault(
                (auteur['nom'], auteur['prenom']), []).append(article)


    document = dict(
        amendements_by_auteur_by_groupe=amendements_by_auteur_by_groupe,
        amendements_from_gouvernement=amendements_from_gouvernement,
        sections=sections,
        titre="Amendements en commission des lois de l'Assemblée (première lecture)",
        )

    DocumentCompleter.complete_document(
        document,
        'amendements-1-assemblee-commission',
        )

    class MyMarkdownWriter(MarkdownWriter):
        article_template = article_markdown_template
        document_template = document_markdown_template

        def article_to_markdown(self, article):
            markdown_article = super().article_to_markdown(article)
            markdown_article.update(dict(
                exposeSommaire = self.paragraphs_to_markdown(article.get('exposeSommaire')),
                place = self.paragraphs_to_markdown(article.get('place')),
                ))
            return markdown_article

        def write_document(self, document, dir):
            super().write_document(document, dir)
            document_dir = os.path.join(dir, document['slug'])
            self.write_data(
                document,
                document,
                os.path.join(document_dir, 'index-amendements-par-auteur.md'),
                self.new_template(amendements_by_auteur_markdown_template),
                )

    markdown_writer = MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
