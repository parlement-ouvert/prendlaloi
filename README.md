# PrendLaLoi

## Convertis un dossier législatif en Markdown

Ce projet est un ensemble de scripts convertissant en Markdown les différents documents constituant un dossier législatif (projet de loi, étude d'impact, loi existantes, avis, etc).

Ces scripts sont un des composants de la [chaîne d'outils permettant de proposer un dossier législatif collaboratif](https://donnees-personnelles.parlement-ouvert.fr/documentation-technique/fonctionnement).

Ce projet est un [logiciel libre](LICENSE), développé par l'équipe de la députée [Paula Forteza](https://forteza.fr) dans le cadre du [bureau ouvert](https://forteza.fr/index.php/2017/10/26/bureau-ouvert/)

# Exemple d'utilisation

Pour un exemple d'utilisation, référez-vous à la documentation technique [documentation technique du dossier législatif du projet de loi relatif à la protection des données personnelles](https://donnees-personnelles.parlement-ouvert.fr/documentation-technique/conversion-markdown)