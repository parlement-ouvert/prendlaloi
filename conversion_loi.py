#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Script de conversion en Markdown d'une loi publiée sur Legifrance"""


import argparse
import json
import os
import re
import sys
import urllib.parse

from lxml import etree

from prendlaloi import (
    DocumentCompleter,
    element_to_html,
    element_to_inner_html,
    element_to_text,
    MarkdownWriter,
    to_slug,
    )


article_markdown_template = '''\
<!-- TITLE: ${numero or titre} -->
% if numero and titre:
<!-- SUBTITLE: ${titre} -->
% endif

[En savoir plus...](${article.url_en_savoir_plus})

## Historique

% if historique:
    % for changement in historique:
* ${changement['type']} [${changement['titre']}](${changement['url']})
    % endfor
% endif

----

% if paragraphes:
    % if isinstance(paragraphes, str):
${paragraphes}
    % else:
        % for paragraphe in paragraphes:
${paragraphe}
        % endfor
    % endif
% endif
'''


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

* [Entête](${url_path}/entete)
* [Visas](${url_path}/visas)
${sections_tree(sections, 0)}
* [Signataires](${url_path}/signataires)
* [Nota](${url_path}/nota)
'''


entete_markdown_template = '''\
<!-- TITLE: Entête -->

${document['titre']}

% for ligne in entete:
${ligne}
% endfor
'''


nota_markdown_template = '''\
<!-- TITLE: Nota -->

${nota}
'''


signataires_markdown_template = '''\
<!-- TITLE: Signataires -->

${signataires}
'''


visas_markdown_template = '''\
<!-- TITLE: Visas -->

${visas}
'''


session_re = re.compile(r';jsessionid=.*?(/|\?|$)')
titre_re = re.compile(r'(?P<categorie>LOI|Loi) n° (?P<numero>[\d]+-[\d]+) du ')
url_base = 'https://www.legifrance.gouv.fr/'
wrong_p_re = re.compile(r'(?m)^<p/>(.*)<p/>$')


def element_to_section(ul):
    assert ul.tag == 'ul', element_to_html(ul)
    assert len(ul) == 1, element_to_html(ul)
    li = ul[0]
    assert li.tag == 'li', element_to_html(li)

    article = None
    articles = None
    etat = None
    ids_article = None
    section = None
    sous_sections = None
    for element in li:
        if etat is None:
            classe = element.get('class')
            if classe in ('titreSection', 'titreSection_abroge'):
                assert element.tag == 'div'
                text = element.text
                if ':' in text:
                    numero, titre = [
                        fragment.strip()
                        for fragment in text.split(':', 1)
                        ]
                else:
                    numero = None
                    titre = text.strip()
                articles = []
                section = dict(
                    abroge=(classe == 'titreSection_abroge'),
                    articles=articles,
                    id=element.attrib['id'],
                    numero=numero,
                    titre=titre,
                    )
                etat = 'apres_titre'
                continue
        if etat == 'apres_titre':
            if element.tag == 'a':
                ids_article = [element.attrib['id']]
                etat = 'apres_id'
                continue
            if element.tag == 'ul':
                sous_sections = []
                section['sections'] = sous_sections
                sous_sections.append(element_to_section(element))
                etat = 'apres_sous_section'
                continue
        if etat == 'apres_id':
            if element.tag == 'a':
                ids_article.append(element.attrib['id'])
                continue
            if element.tag == 'div' and element.get('class') == 'article':
                article_abroge = False
                etat_article = None
                historique_article = []
                ids_article = []
                paragraphes_article = []
                for element_article in element:
                    if etat_article is None:
                        if element_article.get('class') == 'titreArt':
                            assert element_article.tag == 'div'
                            titre_article = element_article.text.strip()
                        elif element_article.get('class') == 'titreArt gris':
                            assert element_article.tag == 'div'
                            article_abroge = True
                            titre_article = element_article.text.strip()
                        else:
                            raise Exception("Élément d'article inattendu dans l'état {} : {}".format(
                                etat_article,
                                element_to_html(element_article),
                                ))
                        assert len(element_article) <= 1, element_to_html(element_article)
                        if len(element_article) == 0:
                            url_en_savoir_plus_article = None
                        else:
                            en_savoir_plus_a = element_article[0]
                            assert en_savoir_plus_a.tag == 'a'
                            assert en_savoir_plus_a.text.strip() == 'En savoir plus sur cet article...'
                            url_en_savoir_plus_article = urllib.parse.urljoin(
                                url_base, en_savoir_plus_a.attrib['href'])
                        etat_article = 'historique'
                    elif etat_article == 'historique' and element_article.get('class') == 'histoArt':
                        assert element_article.tag == 'div'
                        assert len(element_article) == 1
                        assert element_article[0].tag == 'ul'
                        for changement_li in element_article.iterfind('ul/li'):
                            type_changement = changement_li.text.strip()
                            assert type_changement in (
                                'Abrogé par',
                                'Créé par',
                                'Modifié par',
                                ), changement_li.text.strip()
                            assert len(changement_li) <= 2, element_to_html(changement_li)
                            element_changement = changement_li[0]
                            if element_changement.tag == 'a':
                                assert element_changement.attrib['class'] == 'liensArtResolu'
                                url_changement = urllib.parse.urljoin(
                                    url_base, element_changement.attrib['href'])
                            else:
                                element_changement.tag == 'span'
                                assert element_changement.attrib['class'] == 'liensArtNonResolu'
                                url_changement = None
                            if len(changement_li) == 2:
                                assert changement_li[1].tag == 'br'
                            historique_article.append(dict(
                                titre=element_changement.text.strip(),
                                type=type_changement,
                                url=url_changement,
                                ))
                        etat_article = 'paragraphe'
                    elif etat_article in ('historique', 'paragraphe'):
                        paragraphes_article.append(element_to_html(element_article, with_tail=True))
                        etat_article = 'paragraphe'
                    else:
                        raise Exception("Élément d'article inattendu dans l'état {} : {}".format(
                            etat_article,
                            element_to_html(element_article),
                            ))
                article = dict(
                    abroge=article_abroge,
                    historique=historique_article,
                    ids=ids_article,
                    numero=titre_article,
                    paragraphes='\n'.join(paragraphes_article),
                    url_en_savoir_plus=url_en_savoir_plus_article,
                    )
                articles.append(article)
                continue
            if element.tag == 'ul':
                sous_sections = []
                section['sections'] = sous_sections
                sous_sections.append(element_to_section(element))
                etat = 'apres_sous_section'
                continue
        if etat == 'apres_sous_section':
            if element.tag == 'ul':
                sous_sections.append(element_to_section(element))
                continue
        raise Exception("Élément de niveau inattendu dans l'état {} : {}".format(
            etat,
            element_to_html(element),
            ))
    return section


def element_to_text(element):
    return etree.tostring(element, encoding=str, method='text').strip()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-j',
        '--json',
        help='save a JSON conversion of the HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-r',
        '--repair',
        help='save a repaired version of HTML file',
        action='store_true',
        )
    parser.add_argument(
        'html_file',
        help='path of source HTML file',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    parser = etree.HTMLParser()
    with open(args.html_file, 'r', encoding='utf-8') as html_file:
        html = html_file.read()

    # Repair HTML string.
    html = wrong_p_re.sub(r'<p>\1</p>', html)

    root_element = etree.fromstring(html, parser)

    # Repair HTML tree.
    for element in root_element.xpath('.//a[@href]'):
        href = element.attrib['href']
        href  = urllib.parse.urljoin(url_base, href)
        href = session_re.sub(r'\1', href)
        element.attrib['href'] = href
    for element in root_element.xpath('.//br[@clear]'):
        del element.attrib['clear']
    for element in root_element.xpath('.//p[@align]'):
        del element.attrib['align']

    if args.repair:
        tree = root_element.getroottree()
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        tree.write(html_file_path_core + '_repaired' + html_file_extension, encoding='utf-8')

    loi_div = root_element.find('.//div[@class="data"]/div')

    entete_loi_div = loi_div.find('div[1]')
    titre_loi_span = entete_loi_div.find('span')
    titre_loi = element_to_text(titre_loi_span)
    entete_loi = []
    for version_loi_br in titre_loi_span.xpath('following-sibling::br'):
        version_loi = version_loi_br.tail.strip()
        if version_loi:
            entete_loi.append(version_loi)

    enveloppe_loi_div = loi_div.find('div[2]')

    visas_loi_div = enveloppe_loi_div.find('div[1]')
    visas_loi = element_to_html(visas_loi_div)

    contenu_loi_div = enveloppe_loi_div.find('div[2]')
    sections = [
        element_to_section(ul)
        for ul in contenu_loi_div.iterfind('ul')
        ]

    signataires_loi_div = enveloppe_loi_div.find('div[3]')
    signataires_loi = element_to_html(signataires_loi_div)

    nota_loi_div = enveloppe_loi_div.find('div[4]')
    assert nota_loi_div.get('class') == 'article'
    del nota_loi_div.attrib['class']
    nota_loi = element_to_html(nota_loi_div)

    match = titre_re.match(titre_loi)
    assert match is not None, titre_loi

    document = dict(
        categorie=match.group('categorie').lower(),
        entete=entete_loi,
        nota=nota_loi,
        numero=match.group("numero"),
        sections=sections,
        signataires=signataires_loi,
        slug=to_slug('{}-{}'.format(match.group('categorie'), match.group("numero"))),
        titre=titre_loi,
        visas=visas_loi,
        )
    DocumentCompleter.complete_document(
        document,
        to_slug('{}-{}'.format(match.group('categorie'), match.group("numero"))),
        )

    if args.json:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        with open(html_file_path_core + '.json', 'w', encoding='utf-8') as json_file:
            json.dump(document, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    class MyMarkdownWriter(MarkdownWriter):
        article_template = article_markdown_template
        document_template = document_markdown_template

        def write_document(self, document, dir):
            super().write_document(document, dir)
            document_dir = os.path.join(dir, document['slug'])
            self.write_data(
                document,
                dict(
                    entete=self.paragraphs_to_markdown(document['entete']),
                    ),
                os.path.join(document_dir, 'entete.md'),
                self.new_template(entete_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    nota=self.paragraph_to_markdown(document['nota']),
                    ),
                os.path.join(document_dir, 'nota.md'),
                self.new_template(nota_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    signataires=self.paragraph_to_markdown(document['signataires']),
                    ),
                os.path.join(document_dir, 'signataires.md'),
                self.new_template(signataires_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    visas=self.paragraph_to_markdown(document['visas']),
                    ),
                os.path.join(document_dir, 'visas.md'),
                self.new_template(visas_markdown_template),
                )
    markdown_writer= MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
