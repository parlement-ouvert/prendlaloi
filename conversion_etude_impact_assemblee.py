#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Script de conversion en Markdown d'une étude d'impact publiée sur le site de l'Assemblée nationale"""


import argparse
from itertools import chain
import json
import os
import re
import sys

from lxml import etree

from prendlaloi import (
    DocumentCompleter,
    element_to_html,
    element_to_inner_html,
    element_to_text,
    MarkdownWriter,
    to_slug,
    )


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

* [Entête](${url_path}/entete)
${sections_tree(sections, 0)}
* [Notes](${url_path}/notes)
'''


entete_markdown_template = '''\
<!-- TITLE: Entête -->

% for ligne in entete:
${ligne}
% endfor
'''


notes_markdown_template = '''\
<!-- TITLE: Notes -->

% if notes:
    % for paragraphes in notes:
        % for paragraphe in paragraphes:
${paragraphe}
        % endfor
    % endfor
% endif
'''


margin_re = re.compile(r'margin-left: \d+pt(; margin-right: \d+pt)?$')


def iter_lines(element):
    if isinstance(element, etree._Comment):
        pass
    elif element.tag == 'div' and element.get('style') in (
            None,
            'margin-left: 1pt',
            'margin-left: 2%; margin-right: 2%; margin-top: 2%; width: 95%',
            ):
        for child in element:
            yield from iter_lines(child)
    else:
        yield element


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-j',
        '--json',
        help='save a JSON conversion of the HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-n',
        '--number',
        help='document number',
        required=True,
        )
    parser.add_argument(
        '-r',
        '--repair',
        help='save a repaired version of HTML file',
        action='store_true',
        )
    parser.add_argument(
        'html_file',
        help='path of source HTML file',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    parser = etree.HTMLParser()
    with open(args.html_file, 'r', encoding='iso-8859-1') as html_file:
        html = html_file.read()

    # Repair HTML string.
    html = html.replace('; color: #00000a', '')
    html = html.replace('; color: Black', '')
    html = html.replace("font-family: 'Calibri'; ", '')
    html = html.replace("font-family: 'SimSun-ExtB'; ", '')
    html = html.replace("font-family: 'Times New Roman'; ", '')
    html = html.replace("font-family: 'Times New Roman Gras'; ", '')
    html = html.replace("font-family: 'Univers (WN)'; ", '')
    # html = html.replace('font-size: 12pt', '')
    html = html.replace(' style=""', '')
    html = html.replace(
        '<p><b><span style="font-size: 12pt"><a name="P550_28911"></a>CHAPITRE',
        '<p style="text-align: center"><b><span style="font-size: 14pt"><a name="P550_28911"/></a>CHAPITRE',
        )
    html = html.replace(
        '<p><b><span style="font-size: 12pt"><a name="P551_28923"></a>DISPOSITIONS',
        '<p style="text-align: center"><b><span style="font-size: 14pt"><a name="P551_28923"/></a>DISPOSITIONS',
        )
    html = html.replace(
        '<p style="text-align: center"><b><span style="font-size: 11pt">(actuelles',
        '<p><b><span style="font-size: 11pt">(actuelles',
    )

    root_element = etree.fromstring(html, parser)

    # Repair HTML tree.
    for element in root_element.xpath('.//p[@style="text-align: justify"]'):
        del element.attrib['style']

    if args.repair:
        tree = root_element.getroottree()
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        tree.write(html_file_path_core + '_repaired' + html_file_extension, encoding='utf-8')

    titre_document = "Étude d'impact - " + root_element.findtext('./head/title').strip()

    document_div = root_element.find('.//div[@style="margin-left: 2%; margin-right: 2%; margin-top: 2%; width: 95%"]')

    article = None
    articles = None
    chapitre = None
    chapitres = None
    entete = []
    etat = None
    note = None
    notes = None
    paragraphes = None
    section = None
    sections = []
    for element in iter_lines(document_div):
        if etat is None:
            if element.tag in ('a', 'br'):
                pass
            elif element.tag == 'br':
                if entete:
                    entete.append(element_to_inner_html(element))
            elif element.tag == 'p':
                assert element.get('style') == 'text-align: center', etree.tostring(
                    element, encoding=str, method='html').strip()
                del element.attrib['style']
                if len(element) == 0:
                    text = (element.text or '').strip()
                    if text or entete:
                        entete.append(element_to_inner_html(element))
                else:
                    assert not element.text, element_to_html(element)
                    if len(element) == 1 and element_to_text(element) == 'TABLE DES MATIÈRES':
                        etat = 'table_des_matieres'
                    else:
                        entete.append(element_to_inner_html(element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'table_des_matieres':
            if element.tag == 'p':
                style = element.get('style')
                if style is None:
                    pass
                elif style == 'text-align: center':
                    paragraphes = []
                    section = dict(
                        paragraphes=paragraphes,
                        titre=element_to_text(element),
                        )
                    sections.append(section)
                    etat = 'section'
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'section':
            if element.tag == 'div':
                if margin_re.match(element.get('style')) is not None:
                    paragraphes.append(element_to_html(element))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'hr':
                break
            elif element.tag == 'p':
                style = element.get('style')
                if style is None:
                    paragraphes.append(element_to_html(element))
                elif style == 'text-align: center':
                    titre = element_to_text(element)
                    paragraphes = []
                    if titre.startswith('TITRE'):
                        articles = []
                        chapitres = []
                        section = dict(
                            articles=articles,
                            numero=titre,
                            paragraphes=paragraphes,
                            sections=chapitres,
                            )
                        sections.append(section)
                        etat = 'titre_titre'
                    else:
                        section = dict(
                            paragraphes=paragraphes,
                            titre=titre,
                            )
                        sections.append(section)
                        etat = 'section'
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'table':
                paragraphes.append(element_to_html(element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'titre_titre':
            if element.tag == 'p' and element.get('style') == 'text-align: center':
                section['titre'] = element_to_text(element)
                etat = 'contenu_titre'
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'contenu_titre':
            if element.tag == 'p' and element.get('style') == 'text-align: center':
                titre = element_to_text(element)
                paragraphes = []
                if titre.startswith('CHAPITRE'):
                    articles = []
                    chapitre = dict(
                        articles=articles,
                        numero=titre,
                        paragraphes=paragraphes,
                        )
                    chapitres.append(chapitre)
                    etat = 'titre_chapitre'
                elif titre.startswith('ARTICLE'):
                    article = dict(
                        numero=titre,
                        paragraphes=paragraphes,
                        )
                    articles.append(article)
                    etat = 'titre_article'
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'titre_chapitre':
            if element.tag == 'p' and element.get('style') == 'text-align: center':
                chapitre['titre'] = element_to_text(element)
                etat = 'contenu_chapitre'
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'contenu_chapitre':
            if element.tag == 'p' and element.get('style') == 'text-align: center':
                titre = element_to_text(element)
                paragraphes = []
                if titre.startswith('ARTICLE'):
                    article = dict(
                        numero=titre,
                        paragraphes=paragraphes,
                        )
                    articles.append(article)
                    etat = 'titre_article'
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'titre_article':
            if element.tag == 'p':
                style = element.get('style')
                if style is None:
                    # Article without title
                    article['titre'] = None
                    paragraphes.append(element_to_html(element))
                    etat = 'contenu_article'
                elif style == 'text-align: center':
                    article['titre'] = element_to_text(element)
                    etat = 'contenu_article'
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'contenu_article':
            if element.tag == 'div':
                if margin_re.match(element.get('style')) is not None:
                    paragraphes.append(element_to_html(element))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'hr':
                break
            elif element.tag == 'p':
                style = element.get('style')
                if style is None:
                    paragraphes.append(element_to_html(element))
                elif style == 'text-align: center':
                    titre = element_to_text(element)
                    paragraphes = []
                    if titre.startswith('TITRE'):
                        articles = []
                        chapitres = []
                        section = dict(
                            articles=articles,
                            numero=titre,
                            paragraphes=paragraphes,
                            sections=chapitres,
                            )
                        sections.append(section)
                        etat = 'titre_titre'
                    elif titre.startswith('CHAPITRE'):
                        articles = []
                        chapitre = dict(
                            articles=articles,
                            numero=titre,
                            paragraphes=paragraphes,
                            )
                        chapitres.append(chapitre)
                        etat = 'titre_chapitre'
                    elif titre.startswith('ARTICLE'):
                        article = dict(
                            numero=titre,
                            paragraphes=paragraphes,
                            )
                        articles.append(article)
                        etat = 'titre_article'
                    elif titre.startswith('ANNEXE'):
                        section = dict(
                            numero=titre,
                            paragraphes=paragraphes,
                            )
                        sections.append(section)
                        etat = 'titre_annexe'
                    else:
                        raise Exception("Élément inattendu dans l'état {} : {}".format(
                            etat,
                            element_to_html(element),
                            ))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'table':
                paragraphes.append(element_to_html(element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'titre_annexe':
            if element.tag == 'p' and element.get('style') == 'text-align: center':
                section['titre'] = element_to_text(element)
                etat = 'contenu_annexe'
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'contenu_annexe':
            if element.tag == 'div':
                if margin_re.match(element.get('style')) is not None:
                    paragraphes.append(element_to_html(element))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'hr':
                break
            elif element.tag == 'p':
                style = element.get('style')
                if style is None:
                    if len(element) == 0:
                        paragraphes.append(element_to_html(element))
                    elif element[0].tag == 'sup':
                        note = [element_to_html(element)]
                        notes = [note]
                        etat = 'note'
                    else:
                        paragraphes.append(element_to_html(element))
                elif style == 'text-align: center':
                    titre = element_to_text(element)
                    paragraphes = []
                    if titre.startswith('ANNEXE'):
                        section = dict(
                            numero=titre,
                            paragraphes=paragraphes,
                            )
                        sections.append(section)
                        etat = 'titre_annexe'
                    else:
                        raise Exception("Élément inattendu dans l'état {} : {}".format(
                            etat,
                            element_to_html(element),
                            ))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'table':
                paragraphes.append(element_to_html(element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'note':
            if element.tag == 'hr':
                break
            elif element.tag == 'p':
                style = element.get('style')
                if style is None:
                    child = element[0]
                    if child.tag == 'sup' or len(child) > 0 and child[0].tag == 'sup':
                        note = [element_to_html(element)]
                        notes.append(note)
                    else:
                        note.append(element_to_html(element))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        else:
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(element),
                ))

    document = dict(
        categorie="étude d'impact",
        entete=entete,
        numero=args.number,
        notes=notes,
        sections=sections,
        titre=titre_document,
        )
    DocumentCompleter.complete_document(
        document,
        to_slug('etude-impact-{}'.format(args.number)),
        )

    if args.json:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        with open(html_file_path_core + '.json', 'w', encoding='utf-8') as json_file:
            json.dump(document, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    class MyMarkdownWriter(MarkdownWriter):
        document_template = document_markdown_template

        def write_document(self, document, dir):
            super().write_document(document, dir)
            document_dir = os.path.join(dir, document['slug'])
            self.write_data(
                document,
                dict(
                    entete=self.paragraphs_to_markdown(document['entete']),
                    ),
                os.path.join(document_dir, 'entete.md'),
                self.new_template(entete_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    notes=[
                        self.paragraphs_to_markdown(paragraphes)
                        for paragraphes in document['notes']
                        ],
                    ),
                os.path.join(document_dir, 'notes.md'),
                self.new_template(notes_markdown_template),
                )
    markdown_writer= MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
