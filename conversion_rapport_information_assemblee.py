#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Script de conversion en Markdown d'un rapport d'information déposé à l'Assemblée nationale"""


import argparse
from itertools import chain
import json
import os
import re
import sys

from lxml import etree

from prendlaloi import (
    DocumentCompleter,
    element_to_html,
    element_to_inner_html,
    element_to_text,
    MarkdownWriter,
    to_slug,
    )


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

* [Entête](${url_path}/entete)
${sections_tree(sections, 0)}
* [Notes](${url_path}/notes)
'''


entete_markdown_template = '''\
<!-- TITLE: Entête -->

% for ligne in entete:
${ligne}
% endfor
'''


notes_markdown_template = '''\
<!-- TITLE: Notes -->

% if notes:
    % for paragraphes in notes:
        % for paragraphe in paragraphes:
${paragraphe}
        % endfor
    % endfor
% endif
'''


annexe_re = re.compile(r'annexe\s+(n°\s*)?(?P<numero>\d+)\s+:')
headings_re = [
    re.compile(r'(?ims)(?P<numero>\d+)(\s*[-.:\)]\s+(?P<titre>.+?))?\s+\d+\s*$'),
    re.compile(r'(?ims)(?P<numero>[a-z])(\s*[-.:\)]\s+(?P<titre>.+?))?\s+\d+\s*$'),
    re.compile(r'(?ims)(?P<numero>[ilvx]+)(\s*[-.:\)]\s+(?P<titre>.+?))?\s+\d+\s*$'),
    re.compile(r'(?ims)(?P<numero>annexe\s+((n°|n)\s*)?\d+)(\s+:\s+(?P<titre>.+?))?\s+\d+\s*$'),
    # The following regexp must be the last one.
    re.compile(r'(?ims)(?P<titre>.+?)\s+\d+\s*$'),
    ]
margin_re = re.compile(r'margin-left: \d+pt(; margin-right: \d+pt)?$')


def cleanup_node(node):
    node.pop('ligne', None)
    node.pop('parent', None)
    children = node.get('children')
    if children is not None:
        for child in children:
            if child.get('children'):
                node.setdefault('sections', []).append(child)
            else:
                node.setdefault('sections', []).append(child)
            cleanup_node(child)
        del node['children']


def find_next_node_by_anchor(node, anchor):
    children = node.get('children')
    if children is not None:
        child = children[0]
        if child['ancre'] == anchor:
            return child
    return find_next_node_by_anchor_1(node, anchor)


def find_next_node_by_anchor_1(node, anchor):
    parent = node.get('parent')
    if parent is None:
        return None
    parent_children = parent['children']
    node_index = parent_children.index(node)
    if node_index + 1 < len(parent_children):
        sibling = parent_children[node_index + 1]
        if sibling['ancre'] == anchor:
            return sibling
    return find_next_node_by_anchor_1(parent, anchor)


def find_next_node_by_text(node, text):
    text = ' '.join(text.split())
    children = node.get('children')
    if children is not None:
        child = children[0]
        if child['ligne'] == text:
            return child
    return find_next_node_by_text_1(node, text)


def find_next_node_by_text_1(node, text):
    parent = node.get('parent')
    if parent is None:
        return None
    parent_children = parent['children']
    node_index = parent_children.index(node)
    if node_index + 1 < len(parent_children):
        sibling = parent_children[node_index + 1]
        if sibling['ligne'] == text:
            return sibling
    return find_next_node_by_text_1(parent, text)


def iter_lines(element):
    if isinstance(element, etree._Comment):
        pass
    elif element.tag == 'div':
        iter_element = False
        style = element.get('style')
        if style is None:
            iter_element = True
        else:
            styles = dict(
                stripped_item.split(': ')
                for stripped_item in (
                    item.strip()
                    for item in style.split(';')
                    )
                if stripped_item
                )
            if set(styles.keys()).issubset({'margin-left', 'margin-right', 'margin-top', 'width'}):
                iter_element = True
        if iter_element:
            for child in element:
                yield from iter_lines(child)
        else:
            yield element
    else:
        yield element


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-e',
        '--encoding',
        help='encoding of the HTML file',
        default='utf-8',
        )
    parser.add_argument(
        '-j',
        '--json',
        help='save a JSON conversion of the HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-n',
        '--number',
        help='document number',
        required=True,
        )
    parser.add_argument(
        '-r',
        '--repair',
        help='save a repaired version of HTML file',
        action='store_true',
        )
    parser.add_argument(
        'html_file',
        help='path of source HTML file',
        )
    parser.add_argument(
        'table_of_contents_file',
        help='path of text file containing an indented table of contents',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    with open(args.table_of_contents_file, 'r', encoding='utf-8') as table_of_contents_file:
        current_indent = -1
        document = {}
        current_node = document
        for line in table_of_contents_file:
            line = line.rstrip()
            if not line:
                continue
            heading = line.lstrip()
            heading = ' '.join(heading.split())
            for heading_re in headings_re:
                heading_match = heading_re.match(heading)
                if heading_match is not None:
                    heading_groups = heading_match.groupdict()
                    numero = heading_groups.get('numero')
                    titre = heading_groups.get('titre')
                    break
            else:
                raise Exception('Invalid heading: {}'.format(heading))
            heading = heading
            node = dict(
                ligne=heading,
                numero=numero,
                titre=titre,
                )
            indent = len(line) - len(heading)
            if indent > current_indent:
                current_node['children'] = [node]
                current_node['children_indent'] = indent
                node['parent'] = current_node
            else:
                parent = current_node
                while True:
                    parent = parent['parent']
                    assert indent <= parent['children_indent']
                    if indent == parent['children_indent']:
                        break
                parent['children'].append(node)
                node['parent'] = parent
            current_indent = indent
            current_node = node

    parser = etree.HTMLParser()
    with open(args.html_file, 'r', encoding=args.encoding) as html_file:
        html = html_file.read()

    # Repair HTML string.
    html = html.replace('; color: #00000a', '')
    html = html.replace('; color: Black', '')
    html = html.replace("font-family: 'Arial'; ", '')
    html = html.replace("font-family: 'Calibri'; ", '')
    html = html.replace("font-family: 'SimSun-ExtB'; ", '')
    html = html.replace("font-family: 'Times New Roman'; ", '')
    html = html.replace("font-family: 'Times New Roman Gras'; ", '')
    html = html.replace("font-family: 'Univers (WN)'; ", '')
    # html = html.replace('font-size: 12pt', '')
    html = html.replace(' style=""', '')

    root_element = etree.fromstring(html, parser)

    # Repair HTML tree.
    for element in root_element.xpath('.//p[@style="text-align: justify"]'):
        del element.attrib['style']
    for element in root_element.iterfind('.//span'):
        if not element.text and len(element) == 0 and not element.tail:
            element.getparent().remove(element)

    if args.repair:
        tree = root_element.getroottree()
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        tree.write(html_file_path_core + '_repaired' + html_file_extension, encoding='utf-8')

    titre_document = root_element.findtext('./head/title').strip()

    document_div = root_element.find('.//div[@id="englobe"]')
    if document_div is None:
        document_div = root_element.find(
            './/div[@style="margin-left: 2%; margin-right: 2%; margin-top: 2%; width: 98%"]')

    composition = None
    entete = []
    etat = None
    node = None
    note = None
    notes = None
    paragraphes = None
    for element in iter_lines(document_div):
        if etat is None:
            if element.tag in ('a', 'br'):
                pass
            elif element.tag == 'br':
                if entete:
                    entete.append(element_to_inner_html(element))
            elif element.tag == 'p':
                if element.get('style') == 'text-align: center':
                    del element.attrib['style']
                    text = element_to_text(element)
                    if text.upper() in ('SOMMAIRE', 'TABLE DES MATIÈRES'):
                        node = document
                        etat = 'table_des_matieres'
                    elif text or entete:
                        entete.append(element_to_inner_html(element))
                elif 'composition de la commission figure au verso' in element_to_text(element):
                    pass
                else:
                    composition = element_to_inner_html(element)
                    etat = 'composition'
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'composition':
            if element.tag == 'p':
                assert element.get('style') == 'text-align: center', etree.tostring(
                    element, encoding=str, method='html').strip()
                del element.attrib['style']
                text = element_to_text(element)
                if text.upper() in ('SOMMAIRE', 'TABLE DES MATIÈRES'):
                    node = document
                    etat = 'table_des_matieres'
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'table_des_matieres':
            if element.tag == 'p':
                text = element_to_text(element)
                next_node = find_next_node_by_text(node, text)
                if next_node is None:
                    if text in ('___', 'Pages'):
                        pass
                    else:
                        a = element.find('.//a[@name]')
                        assert a is not None, element_to_html(element)
                        anchor = a.attrib['name']
                        if anchor == document['children'][0]['ancre']:
                            node = document['children'][0]
                            paragraphes = []
                            node['paragraphes'] = paragraphes
                            etat = 'section'
                        else:
                            raise Exception("Élément inattendu dans l'état {} : {}".format(
                                etat,
                                element_to_html(element),
                                ))
                else:
                    node = next_node
                    a = element.find('.//a[@href]')
                    assert a is not None, element_to_html(element)
                    anchor = a.attrib['href']
                    assert anchor.startswith('#')
                    node['ancre'] = anchor[1:]
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'section':
            if element.tag == 'center':
                if not (element.text or '').strip() and len(element) == 1 and element[0].tag == 'table':
                    paragraphes.append(element_to_html(element[0]))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'div':
                if margin_re.match(element.get('style')) is not None:
                    paragraphes.append(element_to_html(element))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            elif element.tag == 'hr':
                break
            elif element.tag == 'p':
                a = element.find('./sup[1]/a[@name]') or element.find('./span[1]/sup[1]/a[@name]') \
                    or element.find('./span[1]/i[1]/sup[1]/a[@name]')
                if a is not None:
                    note = [element_to_html(element)]
                    notes = [note]
                    etat = 'note'
                    continue
                a = element.find('.//a[@name]')
                if a is not None:
                    next_node = find_next_node_by_anchor(node, a.attrib['name'])
                    if next_node is not None:
                        node = next_node
                        paragraphes = []
                        node['paragraphes'] = paragraphes
                        continue
                paragraphes.append(element_to_html(element))
            elif element.tag == 'table':
                paragraphes.append(element_to_html(element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        elif etat == 'note':
            if element.tag == 'div' and not element_to_text(element):
                pass
            elif element.tag == 'hr':
                break
            elif element.tag == 'p':
                style = element.get('style')
                if style is None:
                    child = element[0]
                    if child.tag == 'sup' or len(child) > 0 and child[0].tag == 'sup':
                        note = [element_to_html(element)]
                        notes.append(note)
                    else:
                        note.append(element_to_html(element))
                else:
                    raise Exception("Élément inattendu dans l'état {} : {}".format(
                        etat,
                        element_to_html(element),
                        ))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(element),
                    ))
        else:
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(element),
                ))

    cleanup_node(document)
    document.update(dict(
        categorie="rapport d'information",
        entete=entete,
        numero=args.number,
        notes=notes,
        titre=titre_document,
        ))
    DocumentCompleter.complete_document(
        document,
        to_slug('rapport-information-{}'.format(args.number)),
        )

    if args.json:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        with open(html_file_path_core + '.json', 'w', encoding='utf-8') as json_file:
            json.dump(document, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    class MyMarkdownWriter(MarkdownWriter):
        document_template = document_markdown_template

        def write_document(self, document, dir):
            super().write_document(document, dir)
            document_dir = os.path.join(dir, document['slug'])
            self.write_data(
                document,
                dict(
                    entete=self.paragraphs_to_markdown(document['entete']),
                    ),
                os.path.join(document_dir, 'entete.md'),
                self.new_template(entete_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    notes=[
                        self.paragraphs_to_markdown(paragraphes)
                        for paragraphes in document['notes']
                        ],
                    ),
                os.path.join(document_dir, 'notes.md'),
                self.new_template(notes_markdown_template),
                )
    markdown_writer= MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
