#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Script de conversion en Markdown d'un texte paru au journal officiel de l'Union européenne"""


import argparse
import json
import os
import re
import sys

from lxml import etree

from prendlaloi import (
    DocumentCompleter,
    element_to_html,
    element_to_inner_html,
    element_to_text,
    MarkdownWriter,
    to_slug,
    )


adoption_markdown_template = '''\
<!-- TITLE: Adoption -->

% for paragraphe in adoption:
${paragraphe}
% endfor
'''


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

* [Entête](${url_path}/entete)
* [Visas](${url_path}/visas)
${section_tree(considerants, 0)}
* [Adoption](${url_path}/adoption)
${sections_tree(sections, 0)}
* [Signataires](${url_path}/signataires)
* [Notes](${url_path}/notes)
'''


entete_markdown_template = '''\
<!-- TITLE: Entête -->

% for ligne in entete:
${ligne}
% endfor
'''


notes_markdown_template = '''\
<!-- TITLE: Notes -->

% for paragraphe in notes:
${paragraphe}
% endfor
'''


signataires_markdown_template = '''\
<!-- TITLE: Signataires -->

${signataires}
'''


visas_markdown_template = '''\
<!-- TITLE: Visas -->

% for paragraphe in visas:
${paragraphe}
% endfor
'''


numero_considerant_re = re.compile(r'(?ims)\((?P<numero>\d+)\)$')
titre_re = re.compile(r'(?P<categorie>DIRECTIVE|RÈGLEMENT) \(UE\) (?P<numero>[\d]+/[\d]+) DU PARLEMENT EUROPÉEN ET DU CONSEIL$')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d',
        '--discourse',
        help='embed a Discourse widget for comments in articles',
        action='store_true',
        )
    parser.add_argument(
        '-j',
        '--json',
        help='save a JSON conversion of the HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-r',
        '--repair',
        help='save a repaired version of HTML file',
        action='store_true',
        )
    parser.add_argument(
        'html_file',
        help='path of source HTML file',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    parser = etree.HTMLParser()
    tree = etree.parse(args.html_file, parser)

    if args.repair:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        tree.write(html_file_path_core + '_repaired' + html_file_extension, encoding='utf-8')

    adoption = None
    article = None
    articles = None
    chapitre = None
    chapitres = None
    entete = None
    considerants = None
    considerants_section = None
    etat = None
    notes = None
    paragraphes = None
    section = None
    sections = None
    signataires = None
    titre_document = None
    visas = None
    for element in tree.find('/body'):
        if etat is None:
            if element.tag in ('hr', 'table'):
                continue
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'doc-ti':
                    entete = []
                    titre_document = element_to_text(element)
                    etat = 'entete'
                    continue
        if etat == 'entete':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'doc-ti':
                    entete.append(element_to_inner_html(element))
                    continue
                if classe == 'normal':
                    visas = []
                    del element.attrib['class']
                    visas.append(element_to_html(element))
                    etat = 'visas'
                    continue
        if etat == 'visas':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'normal':
                    del element.attrib['class']
                    visas.append(element_to_html(element))
                    continue
            if element.tag == 'table':
                considerants = []
                considerants_section = dict(
                    articles=considerants,
                    titre='Considérants',
                    )
                tr_list = element.findall('.//tr')
                assert len(tr_list) == 1, element_to_html(element)
                td_list = tr_list[0].findall('.//td')
                assert len(td_list) == 2, element_to_html(element)
                numero_match = numero_considerant_re.match(element_to_text(td_list[0]))
                numero = numero_match.group('numero')
                paragraphes = []
                for cell_element in td_list[1]:
                    assert cell_element.tag == 'p', element_to_html(element)
                    assert cell_element.get('class') == 'normal'
                    del cell_element.attrib['class']
                    paragraphes.append(element_to_html(cell_element))
                considerants.append(dict(
                    numero='Considérant {}'.format(numero),
                    paragraphes=paragraphes,
                    ))
                etat = 'considerants'
                continue
        if etat == 'considerants':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'normal':
                    adoption = []
                    del element.attrib['class']
                    adoption.append(element_to_html(element))
                    etat = 'adoption'
                    continue
            if element.tag == 'table':
                tr_list = element.findall('.//tr')
                assert len(tr_list) == 1, element_to_html(element)
                td_list = tr_list[0].findall('.//td')
                assert len(td_list) == 2, element_to_html(element)
                numero_match = numero_considerant_re.match(element_to_text(td_list[0]))
                numero = numero_match.group('numero')
                paragraphes = []
                for cell_element in td_list[1]:
                    assert cell_element.tag == 'p', element_to_html(element)
                    assert cell_element.get('class') == 'normal'
                    del cell_element.attrib['class']
                    paragraphes.append(element_to_html(cell_element))
                considerants.append(dict(
                    numero='Considérant {}'.format(numero),
                    paragraphes=paragraphes,
                    ))
                continue
        if etat == 'adoption':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'normal':
                    del element.attrib['class']
                    adoption.append(element_to_html(element))
                    continue
                if classe == 'ti-section-1':
                    chapitres = []
                    chapitre = dict(
                        numero=element_to_text(element),
                        )
                    chapitres.append(chapitre)
                    etat = 'titre_chapitre'
                    continue
        if etat == 'titre_chapitre':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'ti-section-2':
                    chapitre['titre'] = element_to_text(element)
                    etat = 'contenu_chapitre'
                    continue
        if etat == 'contenu_chapitre':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'ti-section-1':
                    sections = []
                    chapitre['sections'] = sections
                    section = dict(
                        numero=element_to_text(element),
                        )
                    sections.append(section)
                    etat = 'titre_section'
                    continue
                if classe == 'ti-art':
                    articles = []
                    chapitre['articles'] = articles
                    article = dict(
                        numero=element_to_text(element),
                        )
                    articles.append(article)
                    etat = 'titre_article'
                    continue
        if etat == 'titre_section':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'ti-section-2':
                    section['titre'] = element_to_text(element)
                    etat = 'contenu_section'
                    continue
        if etat == 'contenu_section':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'ti-art':
                    articles = []
                    section['articles'] = articles
                    article = dict(
                        numero=element_to_text(element),
                        )
                    articles.append(article)
                    etat = 'titre_article'
                    continue
        if etat == 'titre_article':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'sti-art':
                    paragraphes = []
                    article['paragraphes'] = paragraphes
                    article['titre'] = element_to_text(element)
                    etat = 'contenu_article'
                    continue
        if etat == 'contenu_article':
            if element.tag == 'div':
                classe = element.get('class')
                if classe == 'final':
                    del element.attrib['class']
                    signataires = element_to_html(element)
                    etat = 'signataires'
                    continue
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'normal':
                    del element.attrib['class']
                    paragraphes.append(element_to_html(element))
                    continue
                if classe == 'ti-section-1':
                    numero = element_to_text(element)
                    if numero.startswith('CHAPITRE'):
                        chapitre = dict(
                            numero=numero,
                            )
                        chapitres.append(chapitre)
                        etat = 'titre_chapitre'
                        continue
                    if numero.startswith('Section'):
                        section = dict(
                            numero=element_to_text(element),
                            )
                        sections.append(section)
                        etat = 'titre_section'
                        continue
                if classe == 'ti-art':
                    article = dict(
                        numero=element_to_text(element),
                        )
                    articles.append(article)
                    etat = 'titre_article'
                    continue
            if element.tag == 'table':
                paragraphes.append(element_to_html(element))
                continue
        if etat == 'signataires':
            if element.tag == 'hr':
                classe = element.get('class')
                if classe == 'note':
                    notes = []
                    etat = 'note'
                    continue
        if etat == 'note':
            if element.tag == 'p':
                classe = element.get('class')
                if classe == 'note':
                    del element.attrib['class']
                    notes.append(element_to_html(element))
                    continue
            if element.tag == 'hr':
                classe = element.get('class')
                if classe == 'doc-end':
                    break
        raise Exception("Élément inattendu dans l'état {} : {}".format(
            etat,
            element_to_html(element),
            ))

    match = titre_re.match(titre_document)
    assert match is not None, titre_document

    document = dict(
        adoption=adoption,
        categorie=match.group('categorie').lower(),
        sections=chapitres,
        considerants=considerants_section,
        entete=entete,
        notes=notes,
        numero=match.group("numero"),
        signataires=signataires,
        titre=titre_document,
        visas=visas,
        )

    class MyDocumentCompleter(DocumentCompleter):
        @classmethod
        def complete_document(cls, document, slug_document):
            super().complete_document(document, slug_document)
            cls.complete_section(document, document['considerants'], document['url_path'])

    MyDocumentCompleter.complete_document(
        document,
        to_slug('{}-{}'.format(match.group('categorie'), match.group("numero"))),
        )

    if args.json:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        with open(html_file_path_core + '.json', 'w', encoding='utf-8') as json_file:
            json.dump(document, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    class MyMarkdownWriter(MarkdownWriter):
        article_template = MarkdownWriter.article_template + '\n\n----\n\n${discourse(url_path)}\n' if args.discourse \
            else MarkdownWriter.article_template
        document_template = document_markdown_template

        def write_document(self, document, dir):
            super().write_document(document, dir)
            document_dir = os.path.join(dir, document['slug'])
            self.write_data(
                document,
                dict(
                    adoption=self.paragraphs_to_markdown(document['adoption']),
                    ),
                os.path.join(document_dir, 'adoption.md'),
                self.new_template(adoption_markdown_template),
                )
            self.write_section(document, document['considerants'], document_dir)
            self.write_data(
                document,
                dict(
                    entete=self.paragraphs_to_markdown(document['entete']),
                    ),
                os.path.join(document_dir, 'entete.md'),
                self.new_template(entete_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    notes=self.paragraphs_to_markdown(document['notes']),
                    ),
                os.path.join(document_dir, 'notes.md'),
                self.new_template(notes_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    signataires=self.paragraphs_to_markdown(document['signataires']),
                    ),
                os.path.join(document_dir, 'signataires.md'),
                self.new_template(signataires_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    visas=self.paragraphs_to_markdown(document['visas']),
                    ),
                os.path.join(document_dir, 'visas.md'),
                self.new_template(visas_markdown_template),
                )
    markdown_writer= MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
