#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Récupère sur Eliasse des amendements et de conversion en pages Markdown"""


import argparse
import json
import logging
import os
import sys

import requests


app_name = os.path.splitext(os.path.basename(__file__))[0]
bibard = '490'
legislature = 15
log = logging.getLogger(app_name)
organe = 'CION_LOIS'
organes = [
    'AN',  # Séance, Hémicycle,
    'CION-CEDU',  # Commission Affaires culturelles et éducation
    'CION_LOIS',  # Commission Lois
    # TODO: Complete.
    ]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='increase output verbosity')
    parser.add_argument(
        'target_dir',
        help='path of target directory containing JSON files',
        )
    args = parser.parse_args()

    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    document_slug = 'amendements-1-assemblee-commission'
    amendements_dir = os.path.join(args.target_dir, document_slug)
    if not os.path.exists(amendements_dir):
        os.mkdir(amendements_dir)
    existing_filenames = set(
        filename
        for filename in os.listdir(amendements_dir)
        if not filename.startswith('.')
        )

    log.info('Retrieving discussion...')
    response = requests.get(
        'http://eliasse.assemblee-nationale.fr/eliasse/discussion.do',
        params=dict(
            # _dc=dc,
            bibard=bibard,
            bibardSuffixe='',
            legislature=legislature,
            organeAbrv=organe,
            # &numAmdt=DN13&page=1&start=0&limit=25

            ),
        )
    data = response.json()
    json_filename = 'discussion.json'
    if json_filename in existing_filenames:
        existing_filenames.remove(json_filename)
    with open(os.path.join(amendements_dir, json_filename), 'w', encoding='utf-8') as json_file:
        json.dump(data, json_file, ensure_ascii=False, indent=2, sort_keys=True)
    amdtsParOrdreDeDiscussion = data['amdtsParOrdreDeDiscussion']

    for resume_amendement in amdtsParOrdreDeDiscussion['amendements']:
        log.info('Retrieving amendment {}...'.format(resume_amendement['numero']))
        response = requests.get(
            'http://eliasse.assemblee-nationale.fr/eliasse/amendement.do',
            params=dict(
                # _dc=dc,
                bibard=bibard,
                bibardSuffixe='',
                legislature=legislature,
                organeAbrv=organe,
                numAmdt=resume_amendement['numero'],
                ),
            )
        data = response.json()
        json_filename = 'amendement-{}.json'.format(resume_amendement['numero'])
        if json_filename in existing_filenames:
            existing_filenames.remove(json_filename)
        with open(os.path.join(amendements_dir, json_filename), 'w', encoding='utf-8') as json_file:
            json.dump(data, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    for json_filename in existing_filenames:
        os.remove(os.path.join(amendements_dir, json_filename))

    return 0


if __name__ == '__main__':
    sys.exit(main())
