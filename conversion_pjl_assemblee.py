#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Script de conversion en Markdown d'un projet de loi publié sur le site de l'Assemblée nationale"""


import argparse
from itertools import chain
import json
import os
import re
import sys

from lxml import etree

from prendlaloi import (
    DocumentCompleter,
    element_to_html,
    element_to_inner_html,
    element_to_text,
    MarkdownWriter,
    to_slug,
    )


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

* [Entête](${url_path}/entete)
% if expose_des_motifs:
* [Exposé des motifs](${url_path}/expose-des-motifs)
* Projet de loi
    % if visas:
  * [Visas](${url_path}/visas)
    % endif
${sections_tree(sections, 1)}
% else:
    % if visas:
* [Visas](${url_path}/visas)
    % endif
${sections_tree(sections, 0)}
% endif
'''


entete_markdown_template = '''\
<!-- TITLE: Entête -->

% for ligne in entete:
${ligne}
% endfor
'''


expose_des_motifs_markdown_template = '''\
<!-- TITLE: Exposé des motifs -->

% for paragraphe in expose_des_motifs:
${paragraphe}
% endfor
'''


visas_markdown_template = '''\
<!-- TITLE: Visas -->

% for paragraphe in visas:
${paragraphe}
% endfor
'''


ancre_re = re.compile(r'<a name=".*?" class="ancre"></a>')
titre_re = re.compile(r'N°\s*(?P<numero>[\d]+)\s+')


def element_to_html(element, strip=True, with_tail=False):
    html = etree.tostring(element, encoding=str, method='html', with_tail=with_tail)
    html = ancre_re.sub('', html)
    if strip:
        html = html.strip()
    return html


def element_to_inner_html(element, strip=True):
    html = ''.join(chain(
        [element.text or ''],
        (
            etree.tostring(child, encoding=str, method='html', with_tail=True)
            for child in element
            ),
        ))
    html = ancre_re.sub('', html)
    if strip:
        html = html.strip()
    return html


def iter_lines(element):
    if isinstance(element, etree._Comment):
        pass
    elif element.tag == 'div':
        iter_element = False
        style = element.get('style')
        if style is None:
            iter_element = True
        else:
            styles = dict(
                stripped_item.split(': ')
                for stripped_item in (
                    item.strip()
                    for item in style.split(';')
                    )
                if stripped_item
                )
            if set(styles.keys()).issubset({'margin-left', 'margin-right', 'margin-top', 'width'}):
                iter_element = True
        if iter_element:
            for child in element:
                yield from iter_lines(child)
        else:
            yield element
    else:
        yield element


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d',
        '--discourse',
        help='embed a Discourse widget for comments in articles',
        action='store_true',
        )
    parser.add_argument(
        '-e',
        '--encoding',
        help='encoding of the HTML file',
        default='utf-8',
        )
    parser.add_argument(
        '-j',
        '--json',
        help='save a JSON conversion of the HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-r',
        '--repair',
        help='save a repaired version of HTML file',
        action='store_true',
        )
    parser.add_argument(
        'html_file',
        help='path of source HTML file',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    parser = etree.HTMLParser()
    with open(args.html_file, 'r', encoding=args.encoding) as html_file:
        html = html_file.read()

    root_element = etree.fromstring(html, parser)

    # Repair HTML tree.
    for element in root_element.xpath('.//p[@style="text-align: justify"]'):
        del element.attrib['style']
    for element in root_element.xpath('''.//span[@style="font-family: 'Times New Roman'; font-size: 14pt"]'''):
        del element.attrib['style']
    for element in root_element.xpath('''.//span[@style="font-family: 'Times New Roman Gras'; font-size: 14pt"]'''):
        del element.attrib['style']
    for element in root_element.xpath('''.//span[@style="font-family: 'Times New Roman Italique'; font-size: 14pt"]'''):
        del element.attrib['style']
    for element in root_element.xpath('''.//span[@style="font-family: 'CG TIMES (WN)'; font-size: 14pt"]'''):
        del element.attrib['style']
    for element in root_element.xpath('''.//span[@style]'''):
        style = element.attrib['style']
        fragment = "font-family: 'Times New Roman'; "
        if fragment in style:
            element.attrib['style'] = style.replace(fragment, '')

    if args.repair:
        tree = root_element.getroottree()
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        tree.write(html_file_path_core + '_repaired' + html_file_extension, encoding='utf-8')

    titre_document = root_element.findtext('.//title').strip()

    document_div = root_element.find('.//div[@class="interieur-contenu-principal"]/div[2]')
    if document_div is None:
        document_div = root_element.find('.//div[@style="margin-left: 2%; margin-right: 2%; margin-top: 2%; "]')

    entete_document = []
    expose_des_motifs = []
    etat = None
    titre_pjl = None
    titres_pjl = []
    visas = []
    for ligne_element in iter_lines(document_div):
        if etat is None:
            if isinstance(ligne_element, etree._Comment):
                pass
            elif ligne_element.tag == 'a':
                pass
            elif ligne_element.tag == 'br':
                if entete_document:
                    entete_document.append(element_to_html(ligne_element))
            elif ligne_element.tag == 'p':
                if ligne_element.get('style') == 'text-align: center':
                    del ligne_element.attrib['style']
                    if len(ligne_element) == 0:
                        text = (ligne_element.text or '').strip()
                        if text or entete_document:
                            entete_document.append(element_to_html(ligne_element))
                    else:
                        text = element_to_text(ligne_element)
                        text_upper = text.upper()
                        if text_upper == 'EXPOSÉ DES MOTIFS':
                            etat = 'expose_des_motifs'
                        elif text_upper.startswith('TITRE'):
                            titre_pjl = dict(
                                numero=text,
                                )
                            titres_pjl.append(titre_pjl)
                            etat = 'titre_pjl'
                        else:
                            entete_document.append(element_to_html(ligne_element))
                else:
                    entete_document.append(element_to_html(ligne_element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(ligne_element),
                    ))
        elif etat == 'expose_des_motifs':
            if isinstance(ligne_element, etree._Comment):
                pass
            elif ligne_element.tag == 'p':
                if ligne_element.get('style') == 'text-align: center':
                    assert len(ligne_element) == 1 and ligne_element[0].text == 'PROJET DE LOI'
                    etat = 'visas'
                else:
                    expose_des_motifs.append(element_to_html(ligne_element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(ligne_element),
                    ))
        elif etat == 'visas':
            if ligne_element.tag == 'p':
                if ligne_element.get('style') == 'text-align: center':
                    text = element_to_text(ligne_element)
                    text_upper = text.upper()
                    if text_upper.startswith('TITRE'):
                        titre_pjl = dict(
                            numero=text,
                            )
                        titres_pjl.append(titre_pjl)
                        etat = 'titre_pjl'
                        continue
                visas.append(element_to_html(ligne_element))
            elif ligne_element.tag == 'table':
                visas.append(element_to_html(ligne_element))
            else:
                raise Exception("Élément inattendu dans l'état {} : {}".format(
                    etat,
                    element_to_html(ligne_element),
                    ))
        elif etat == 'titre_pjl':
            if ligne_element.tag == 'p' and ligne_element.get('style') == 'text-align: center':
                titre_pjl['titre'] = element_to_text(ligne_element)
                titre_pjl['sections'] = chapitres_pjl = []
                titre_pjl['articles'] = articles_pjl = []
                etat = 'numero_chapitre_pjl'
                continue
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(ligne_element),
                ))
        elif etat == 'numero_chapitre_pjl':
            if ligne_element.tag == 'p' and ligne_element.get('style') == 'text-align: center':
                text = element_to_text(ligne_element)
                text_upper = text.upper()
                if text_upper.startswith('CHAPITRE'):
                    chapitre_pjl = dict(
                        numero=text,
                        )
                    chapitres_pjl.append(chapitre_pjl)
                    etat = 'titre_chapitre_pjl'
                    continue
                if text_upper.startswith('ARTICLE'):
                    paragraphes = []
                    article_pjl = dict(
                        numero=text,
                        paragraphes=paragraphes,
                        )
                    articles_pjl.append(article_pjl)
                    etat = 'paragraphes_article_pjl'
                    continue
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(ligne_element),
                ))
        elif etat == 'titre_chapitre_pjl':
            if ligne_element.tag == 'p' and ligne_element.get('style') == 'text-align: center':
                chapitre_pjl['titre'] = element_to_text(ligne_element)
                chapitre_pjl['articles'] = articles_pjl = []
                etat = 'numero_article_pjl'
                continue
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(ligne_element),
                ))
        elif etat == 'numero_article_pjl':
            if ligne_element.tag == 'p' and ligne_element.get('style') == 'text-align: center':
                text = element_to_text(ligne_element)
                text_upper = text.upper()
                if text_upper.startswith('ARTICLE'):
                    paragraphes = []
                    article_pjl = dict(
                        numero=text,
                        paragraphes=paragraphes,
                        )
                    articles_pjl.append(article_pjl)
                    etat = 'paragraphes_article_pjl'
                    continue
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(ligne_element),
                ))
        elif etat == 'paragraphes_article_pjl':
            if ligne_element.tag == 'hr':
                break
            elif ligne_element.tag == 'p':
                if ligne_element.get('style') == 'text-align: center' \
                        and not element_to_text(ligne_element).startswith('«'):
                    text = element_to_text(ligne_element)
                    text_upper = text.upper()
                    if text_upper.startswith('TITRE'):
                        titre_pjl = dict(
                            numero=text,
                            )
                        titres_pjl.append(titre_pjl)
                        etat = 'titre_pjl'
                        continue
                    if text_upper.startswith('CHAPITRE'):
                        chapitre_pjl = dict(
                            numero=text,
                            )
                        chapitres_pjl.append(chapitre_pjl)
                        etat = 'titre_chapitre_pjl'
                        continue
                    if text_upper.startswith('ARTICLE'):
                        paragraphes = []
                        article_pjl = dict(
                            numero=text,
                            paragraphes=paragraphes,
                            )
                        articles_pjl.append(article_pjl)
                        etat = 'paragraphes_article_pjl'
                        continue
                else:
                    paragraphes.append(element_to_html(ligne_element))
                    continue
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(ligne_element),
                ))
        else:
            raise Exception("Élément inattendu dans l'état {} : {}".format(
                etat,
                element_to_html(ligne_element),
                ))

    match = titre_re.match(titre_document)
    assert match is not None, titre_document

    document = dict(
        categorie='pjl',
        entete=entete_document,
        expose_des_motifs=expose_des_motifs,
        numero=match.group("numero"),
        sections=titres_pjl,
        titre=titre_document,
        visas=visas,
        )
    DocumentCompleter.complete_document(
        document,
        to_slug('pjl-{}'.format(match.group("numero"))),
        )

    if args.json:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        with open(html_file_path_core + '.json', 'w', encoding='utf-8') as json_file:
            json.dump(document, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    class MyMarkdownWriter(MarkdownWriter):
        article_template = MarkdownWriter.article_template + '\n\n----\n\n${discourse(url_path)}\n' if args.discourse \
            else MarkdownWriter.article_template
        document_template = document_markdown_template

        def write_document(self, document, dir):
            super().write_document(document, dir)
            document_dir = os.path.join(dir, document['slug'])
            self.write_data(
                document,
                dict(
                    entete=self.paragraphs_to_markdown(document['entete']),
                    ),
                os.path.join(document_dir, 'entete.md'),
                self.new_template(entete_markdown_template),
                )
            if document['expose_des_motifs']:
                self.write_data(
                    document,
                    dict(
                        expose_des_motifs=self.paragraphs_to_markdown(document['expose_des_motifs']),
                        ),
                    os.path.join(document_dir, 'expose-des-motifs.md'),
                    self.new_template(expose_des_motifs_markdown_template),
                    )
            if document['visas']:
                self.write_data(
                    document,
                    dict(
                        visas=self.paragraphs_to_markdown(document['visas']),
                        ),
                    os.path.join(document_dir, 'visas.md'),
                    self.new_template(visas_markdown_template),
                    )

    markdown_writer= MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)


    return 0


if __name__ == '__main__':
    sys.exit(main())
