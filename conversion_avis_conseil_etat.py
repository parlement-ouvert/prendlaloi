#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Script de conversion en Markdown d'un avis du Conseil d'État publié en PDF sur son site

Avant de lancer ce script il faut avoir converti le fichier PDF en HTML par la commande :
```bash
pdftohtml -noframes avis.pdf
```
"""


import argparse
import json
import os
import re
import sys

from lxml import etree

from prendlaloi import (
    DocumentCompleter,
    element_to_html,
    element_to_inner_html,
    element_to_text,
    MarkdownWriter,
    to_slug,
    )


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

${organisme}

${date}

N° ${numero}

${sections_tree(sections, 0)}

% if visa:
${visa}
% endif
'''


numero_article_re = re.compile('\d+\.$')
saut_de_page_re = re.compile(r'''(?ims)
 <br/>
<hr/>
<a name\=[\d]+></a><b>NOR : [A-Z0-9]+</b> <br/>
 <br/>
[\d]+<br/>
<b> </b><br/>
''')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d',
        '--date',
        help='document date',
        required=True,
        )
    parser.add_argument(
        '-j',
        '--json',
        help='save a JSON conversion of the HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-n',
        '--number',
        help='document number',
        required=True,
        )
    parser.add_argument(
        '-r',
        '--repair',
        help='save a repaired version of HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-t',
        '--title',
        help='document title',
        required=True,
        )
    parser.add_argument(
        'html_file',
        help='path of source HTML file',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    parser = etree.HTMLParser()
    with open(args.html_file, 'r', encoding='utf-8') as html_file:
        html = html_file.read()

    # Repair HTML string.
    html = html.replace('&#160;', ' ')
    html = saut_de_page_re.sub('\n', html)
    html = html.replace('<b> <br/>', ' <br/><b>')
    html = html.replace('<b> <br/>', ' <br/><b>')
    html = html.replace(' <br/></b>', '</b> <br/>')
    html = html.replace(' <br/></b>', '</b> <br/>')
    html = html.replace('<i> <br/>', ' <br/><i>')
    html = html.replace('<i> <br/>', ' <br/><i>')
    html = html.replace(' <br/></i>', '</i> <br/>')
    html = html.replace(' <br/></i>', '</i> <br/>')
    html = html.replace('<br/> <br/>', '<BR/> <BR/>')
    html = html.replace('<br/>', '')
    html = html.replace(' <BR/> <BR/>', '<br/>\n<br/>\n')
    html = html.replace('<b> </b>', ' ')
    html = html.replace('<i> </i>', ' ')
    html = html.replace('<b></b>', '')
    html = html.replace('<i></i>', '')
    while True:
        new_html = html.replace('\n ', '\n')
        if new_html == html:
            break
        html = new_html
    html = html.replace('</b>\n<b>', ' ')
    html = html.replace('</i>\n<i>', ' ')
    html = html.replace('<i>Contrôle</i>s<br/>', '<i>Contrôles</i><br/>')

    root_element = etree.fromstring(html, parser)

    if args.repair:
        tree = root_element.getroottree()
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        tree.write(html_file_path_core + '_repaired' + html_file_extension, encoding='utf-8')

    body = root_element.find('.//body')

    article = None
    articles = None
    chapitre = None
    chapitres = None
    etat = None
    visa = None
    for ligne_element in body:
        if etat is None:
            if ligne_element.tag == 'i':
                assert len(ligne_element) == 0
                chapitres = []
                articles = []
                chapitre = dict(
                    articles=articles,
                    titre=ligne_element.text.strip()
                    )
                chapitres.append(chapitre)
                etat = 'chapitre'
                continue
            # Ignore first lines.
            continue
        if etat == 'chapitre':
            if ligne_element.tag == 'br':
                assert not (ligne_element.tail or '').strip()
                continue
            if ligne_element.tag == 'b':
                assert len(ligne_element) == 0
                numero_article = ligne_element.text.strip()
                assert numero_article_re.match(numero_article) is not None
                paragraphes = []
                article = dict(
                    numero=numero_article,
                    paragraphes=paragraphes,
                    )
                articles.append(article)
                paragraphe = ligne_element.tail or ''
                etat = 'paragraphe'
                continue
        if etat == 'paragraphe':
            if ligne_element.tag == 'b':
                assert len(ligne_element) == 0
                paragraphe = paragraphe.strip()
                if paragraphe:
                    paragraphes.append('<p>{}</p>'.format(paragraphe))
                numero_article = ligne_element.text.strip()
                assert numero_article_re.match(numero_article) is not None
                paragraphes = []
                article = dict(
                    numero=numero_article,
                    paragraphes=paragraphes,
                    )
                articles.append(article)
                paragraphe = ligne_element.tail or ''
                continue
            if ligne_element.tag == 'br':
                paragraphe = paragraphe.strip()
                if paragraphe:
                    paragraphes.append('<p>{}</p>'.format(paragraphe))
                paragraphe = ligne_element.tail or ''
                continue
            if ligne_element.tag == 'hr':
                paragraphe = paragraphe.strip()
                if paragraphe:
                    visa = '<p>{}</p>'.format(paragraphe)
                break
            if ligne_element.tag == 'i':
                assert len(ligne_element) == 0
                if not (ligne_element.tail or '').strip() and ligne_element.getnext().tag == 'br':
                    paragraphe = paragraphe.strip()
                    if paragraphe:
                        paragraphes.append('<p>{}</p>'.format(paragraphe))
                    articles = []
                    chapitre = dict(
                        articles=articles,
                        titre=ligne_element.text.strip()
                        )
                    chapitres.append(chapitre)
                    etat = 'chapitre'
                    continue
                paragraphe += element_to_html(ligne_element, strip=False, with_tail=True)
                continue
        raise Exception("Élément inattendu dans l'état {} : {}".format(
            etat,
            element_to_html(ligne_element),
            ))

    document = dict(
        categorie='avis',
        date=args.date,
        numero=args.number,
        organisme="Conseil d'État",
        sections=chapitres,
        titre=args.title,
        visa=visa,
        )
    DocumentCompleter.complete_document(
        document,
        to_slug('avis-conseil-etat-{}'.format(args.number)),
        )

    if args.json:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        with open(html_file_path_core + '.json', 'w', encoding='utf-8') as json_file:
            json.dump(document, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    class MyMarkdownWriter(MarkdownWriter):
        document_template = document_markdown_template

        def document_to_markdown(self, document):
            markdown_document = super().document_to_markdown(document)
            markdown_document.update(dict(
                visa=self.paragraph_to_markdown(document['visa']),
                ))
            return markdown_document
    markdown_writer= MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
