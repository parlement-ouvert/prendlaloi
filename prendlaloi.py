# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import copy
from itertools import chain
import os

import html2text
from lxml import etree
import mako.template
from slugify import slugify


article_markdown_template = '''\
<!-- TITLE: ${numero or titre} -->
% if numero and titre:
<!-- SUBTITLE: ${titre} -->
% endif

% if paragraphes:
    % if isinstance(paragraphes, str):
${paragraphes}
    % else:
        % for paragraphe in paragraphes:
${paragraphe}
        % endfor
    % endif
% endif
'''


document_markdown_template = '''\
<!-- TITLE: ${titre} -->

${sections_tree(sections, 0)}
'''


markdown_template_header = '''\
<%!
def block_trimmer(text):
    if text is None:
        return None
    while '\\n' in text:
        first_line, remaining_lines = text.split('\\n', 1)
        if first_line.strip():
            return text.rstrip()
        text = remaining_lines
    return text.rstrip()


def inliner(text):
    if text is None:
        return None
    return ' '.join(text.split())
%>


<%def name="articles_tree(articles, level)">
    % for article in (articles or []):
${'  ' * level}* [${' — '.join(filter(None, [article.get('numero'), article.get('titre')]))}](${article['url_path']})
    % endfor
</%def>


<%def name="discourse(url_path)" filter="inliner">
<div id='discourse-comments'></div>

<script type="text/javascript">
    DiscourseEmbed = {
        discourseUrl: 'https://forum.parlement-ouvert.fr/',
        discourseEmbedUrl: 'https://donnees-personnelles.parlement-ouvert.fr${url_path}'
    };
    (function() {
        var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
        d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
    })();
</script>
</%def>


<%def name="section_tree(section, level)">
${'  ' * level}* [${' — '.join(filter(None, [section.get('numero'), section.get('titre')]))}](${section['url_path']})
${articles_tree(section.get('articles'), level + 1)}
${sections_tree(section.get('sections'), level + 1)}
</%def>


<%def name="sections_tree(sections, level)">
    % for section in (sections or []):
${section_tree(section, level)}
    % endfor
</%def>
'''


section_markdown_template = '''\
<!-- TITLE: ${numero or titre} -->
% if numero and titre:
<!-- SUBTITLE: ${titre} -->
% endif

% if paragraphes:
    % if isinstance(paragraphes, str):
${paragraphes}
    % else:
        % for paragraphe in paragraphes:
${paragraphe}
        % endfor
    % endif
% endif
${articles_tree(articles, 0)}
${sections_tree(sections, 0)}
'''


class DocumentCompleter:
    @classmethod
    def complete_document(cls, document, slug_document):
        document['slug'] = slug_document
        document['url_path'] = url_path = '/{}'.format(document['slug'])
        cls.complete_sections(document, document['sections'], url_path)

    @classmethod
    def complete_section(cls, document, section, url_path):
        section_slug = to_slug((section.get('numero') or section.get('titre')).split('<', 1)[0])
        if len(section_slug) > 120:
            section_slug = section_slug[:119] + '-'
        if section.get('abroge', False) and 'abroge' not in section_slug:
            section_slug += '-abroge'
        section['slug'] = section_slug
        section['url_path'] = section_url_path = '{}/{}'.format(url_path, section_slug)
        for article in (section.get('articles') or []):
            article_slug = to_slug((article.get('numero') or article.get('titre')).split('<', 1)[0])
            if len(article_slug) > 120:
                article_slug = article_slug[:119] + '-'
            if article.get('abroge', False) and 'abroge' not in article_slug:
                article_slug += '-abroge'
            article['slug'] = article_slug
            article['url_path'] = '{}/{}'.format(section_url_path, article_slug)
        cls.complete_sections(document, section.get('sections'), section_url_path)

    @classmethod
    def complete_sections(cls, document, sections, url_path):
        for section in (sections or []):
            cls.complete_section(document, section, url_path)


class MarkdownWriter:
    article_template = article_markdown_template
    document_template = document_markdown_template
    markdown_maker = None
    section_template = section_markdown_template

    def __init__(self):
        self.article_template = self.new_template(self.article_template)
        self.document_template = self.new_template(self.document_template)
        self.section_template = self.new_template(self.section_template)

        # See https://github.com/Alir3z4/html2text/blob/master/docs/usage.md
        self.markdown_maker = html2text.HTML2Text()
        self.markdown_maker.body_width = 0
        self.markdown_maker.bypass_tables = True
        self.markdown_maker.unicode_snob = True

    def article_to_markdown(self, article):
        markdown_article = copy.copy(article)
        markdown_article.update(dict(
            paragraphes = self.paragraphs_to_markdown(article.get('paragraphes')),
            ))
        return markdown_article

    def document_to_markdown(self, document):
        markdown_document = copy.copy(document)
        return markdown_document

    def new_template(self, template_text):
        return mako.template.Template(
            '{}\n{}'.format(markdown_template_header, template_text),
            # default_filters=['block_trimmer'],
            )

    def paragraph_to_markdown(self, paragraphe):
        return self.markdown_maker.handle(paragraphe).rstrip()

    def paragraphs_to_markdown(self, paragraphes):
        if isinstance(paragraphes, str):
            return self.paragraph_to_markdown(paragraphes)
        return [
            self.paragraph_to_markdown(paragraphe)
            for paragraphe in (paragraphes or [])
            ]

    def section_to_markdown(self, section):
        markdown_section = copy.copy(section)
        markdown_section.update(dict(
            paragraphes = self.paragraphs_to_markdown(section.get('paragraphes')),
            ))
        return markdown_section

    def write_article(self, document, article, dir):
        self.write_data(
            document,
            self.article_to_markdown(article),
            os.path.join(dir, article['slug'] + '.md'),
            self.article_template,
            )

    def write_document(self, document, dir):
        os.makedirs(dir, exist_ok=True)
        self.write_data(
            document,
            self.document_to_markdown(document),
            os.path.join(dir, document['slug'] + '.md'),
            self.document_template,
            )

        document_dir = os.path.join(dir, document['slug'])
        os.makedirs(document_dir, exist_ok=True)
        self.write_sections(document, document['sections'], document_dir)

    def write_data(self, document, data, file_path, template):
        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(template.render_unicode(document=document, **data).replace('£', '#').strip())
            file.write('\n')

    def write_section(self, document, section, dir):
        self.write_data(
            document,
            self.section_to_markdown(section),
            os.path.join(dir, section['slug'] + '.md'),
            self.section_template,
            )

        if section.get('articles') or section.get('sections'):
            section_dir = os.path.join(dir, section['slug'])
            os.makedirs(section_dir, exist_ok=True)
            for article in (section.get('articles') or []):
                self.write_article(document, article, section_dir)

            self.write_sections(document, section.get('sections'), section_dir)

    def write_sections(self, document, sections, dir):
        for section in (sections or []):
            self.write_section(document, section, dir)


def element_to_html(element, strip=True, with_tail=False):
    html = etree.tostring(element, encoding=str, method='html', with_tail=with_tail)
    if strip:
        html = html.strip()
    return html


def element_to_inner_html(element, strip=True):
    html = ''.join(chain(
        [element.text or ''],
        (
            etree.tostring(child, encoding=str, method='html', with_tail=True)
            for child in element
            ),
        ))
    if strip:
        html = html.strip()
    return html


def element_to_text(element):
    return etree.tostring(element, encoding=str, method='text').strip()


slug_replacements = {
    '1er': '1',
    'ier': 'i',
    'ndeg': None,
    }


def to_slug(s):
    return '-'.join(
        replaced_fragment
        for replaced_fragment in (
            slug_replacements.get(fragment, fragment)
            for fragment in slugify(s).split('-')
           )
        if replaced_fragment
        )
