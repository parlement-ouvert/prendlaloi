#! /usr/bin/env python3


# PrendLaLoi -- Convertis un dossier législatif en Markdown.
# By: Emmanuel Raviart <emmanuel@raviart.com>
#
# Copyright (C) 2018 Paula Forteza & Emmanuel Raviart
# https://framagit.org/parlement-ouvert/prendlaloi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Script de conversion en Markdown d'une délibération de la CNIL publiée sur Legifrance"""


import argparse
import json
import os
import re
import sys
import urllib.parse

from lxml import etree

from prendlaloi import (
    DocumentCompleter,
    element_to_html,
    element_to_inner_html,
    element_to_text,
    MarkdownWriter,
    to_slug,
    )


considerants_markdown_template = '''\
<!-- TITLE: Considérants -->

% for paragraphe in considerants:
${paragraphe}
% endfor
'''


document_markdown_template = '''\
<!-- TITLE: ${titre_court} -->
<!-- SUBTITLE: ${organisme} -->

_${titre}_

* [Considérants](${url_path}/considerants)
* [Introduction](${url_path}/introduction)
${sections_tree(sections, 0)}
* [Signataires](${url_path}/signataires)
'''


introduction_markdown_template = '''\
<!-- TITLE: Introduction -->

% for paragraphe in introduction:
${paragraphe}
% endfor
'''


signataires_markdown_template = '''\
<!-- TITLE: Signataires -->

% for paragraphe in signataires:
${paragraphe}
% endfor
'''


strong_strong_re = re.compile(r'(?ims)</strong>\s+<strong>')
titre_re = re.compile(r'(?P<categorie>Délibération) n°(?P<numero>[\d]+-[\d]+) du ')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-j',
        '--json',
        help='save a JSON conversion of the HTML file',
        action='store_true',
        )
    parser.add_argument(
        '-r',
        '--repair',
        help='save a repaired version of HTML file',
        action='store_true',
        )
    parser.add_argument(
        'html_file',
        help='path of source HTML file',
        )
    parser.add_argument(
        'target_dir',
        help='path of target directory containing Markdown text files',
        )
    args = parser.parse_args()

    parser = etree.HTMLParser()
    with open(args.html_file, 'r', encoding='utf-8') as html_file:
        html = html_file.read()

    # Repair HTML string.
    html = strong_strong_re.sub(' ', html)
    html = html.replace("""
<p align="center">
<strong>Chapitre I (Champ d’application)</strong>
</p>
""", """
<p align="center">Chapitre I</p>
<p align="center"><strong>Champ d’application</strong></p>
""")

    root_element = etree.fromstring(html, parser)

    # Repair HTML tree.
    for element in root_element.xpath('.//br[@clear]'):
        del element.attrib['clear']

    if args.repair:
        tree = root_element.getroottree()
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        tree.write(html_file_path_core + '_repaired' + html_file_extension, encoding='utf-8')

    considerants = None
    etat = None
    introduction = None
    organisme = None
    partie = None
    parties = None
    signataires = None
    titre_court_document = None
    titre_document = None
    for element in root_element.find('.//div[@class="data"]'):
        if isinstance(element, etree._Comment):
            continue
        if etat is None:
            if element.tag == 'strong':
                element.tag = 'span'
                organisme = element_to_inner_html(element)
                etat = 'apres_organisme'
                continue
        if etat == 'apres_organisme':
            if element.tag == 'div':
                id = element.get('id')
                if id == "header_cnil":
                    etat_enfant = None
                    for enfant in element:
                        if etat_enfant is None:
                            if enfant.tag == 'div':
                                sous_classe = enfant.get('class')
                                if sous_classe == 'title_cnil' and len(enfant) == 1:
                                    sous_enfant = enfant[0]
                                    if sous_enfant.tag == 'strong':
                                        sous_enfant.tag = 'span'
                                        titre_court_document = element_to_inner_html(sous_enfant)
                                        etat_enfant = 'titre'
                                        continue
                        if etat_enfant == 'titre':
                            if enfant.tag == 'div' and len(enfant) == 1:
                                sous_enfant = enfant[0]
                                if sous_enfant.tag == 'strong':
                                    sous_enfant.tag = 'span'
                                    titre_document = element_to_inner_html(sous_enfant)
                                    etat_enfant = 'apres_titre'
                                    continue
                        raise Exception("Élément inattendu dans l'état {}, sous-état {} : {}".format(
                            etat,
                            etat_enfant,
                            element_to_html(enfant),
                            ))
                    etat = 'contenu'
                    continue
        if etat == 'contenu':
            if element.tag == 'div':
                id = element.get('id')
                if id == "contenu":
                    etat_enfant = None
                    for enfant in element:
                        if etat_enfant is None:
                            if enfant.tag == 'div' and len(enfant) == 1:
                                sous_enfant = enfant[0]
                                if sous_enfant.tag == 'strong':
                                    etat_enfant = 'apres_etat'
                                    continue
                        if etat_enfant == 'apres_etat':
                            if enfant.tag == 'div':
                                considerants = []
                                etat_ligne = None
                                for ligne in enfant:
                                    if etat_ligne is None:
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align is None:
                                                considerants.append(element_to_html(ligne))
                                                continue
                                            if align == 'justify':
                                                introduction = []
                                                del ligne.attrib['align']
                                                introduction.append(element_to_html(ligne))
                                                etat_ligne = 'introduction'
                                                continue
                                    if etat_ligne == 'introduction':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align is None and len(ligne) == 1:
                                                element_ligne = ligne[0]
                                                if element_ligne.tag == 'strong' and len(element_ligne) == 1:
                                                    enfant_ligne = element_ligne[0]
                                                    if enfant_ligne.tag == 'u':
                                                        enfant_ligne.tag = 'span'
                                                        parties = []
                                                        paragraphes = []
                                                        partie = dict(
                                                            paragraphes=paragraphes,
                                                            titre=element_to_inner_html(enfant_ligne),
                                                            )
                                                        parties.append(partie)
                                                        etat_ligne = 'observations_generales'
                                                        continue
                                            if align == 'justify':
                                                del ligne.attrib['align']
                                                introduction.append(element_to_html(ligne))
                                                continue
                                        if ligne.tag == 'ul':
                                            introduction.append(element_to_html(ligne))
                                            continue
                                    if etat_ligne == 'observations_generales':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align is None and len(ligne) == 1:
                                                element_ligne = ligne[0]
                                                if element_ligne.tag == 'strong' and len(element_ligne) == 1:
                                                    enfant_ligne = element_ligne[0]
                                                    if enfant_ligne.tag == 'u':
                                                        enfant_ligne.tag = 'span'
                                                        partie = dict(
                                                            titre=element_to_inner_html(enfant_ligne),
                                                            )
                                                        parties.append(partie)
                                                        etat_ligne = 'examen_par_article'
                                                        continue
                                            if align == 'justify':
                                                del ligne.attrib['align']
                                                paragraphes.append(element_to_html(ligne))
                                                continue
                                    if etat_ligne == 'examen_par_article':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align == 'center':
                                                text = (ligne.text or '').strip()
                                                if text.startswith('TITRE'):
                                                    titres = []
                                                    partie['sections'] = titres
                                                    ligne.tag = 'span'
                                                    del ligne.attrib['align']
                                                    titre = dict(
                                                        numero=element_to_inner_html(ligne),
                                                        )
                                                    titres.append(titre)
                                                    etat_ligne = 'titre_titre'
                                                    continue
                                    if etat_ligne == 'titre_titre':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align == 'center':
                                                if len(ligne) == 0 and not (ligne.text or '').strip():
                                                    continue
                                                if len(ligne) == 1:
                                                    element_ligne = ligne[0]
                                                    if element_ligne.tag == 'strong':
                                                        element_ligne.tag = 'span'
                                                        titre['titre'] = element_to_inner_html(element_ligne)
                                                        etat_ligne = 'contenu_titre'
                                                        continue
                                    if etat_ligne == 'contenu_titre':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align == 'center':
                                                chapitres = []
                                                titre['sections'] = chapitres
                                                ligne.tag = 'span'
                                                del ligne.attrib['align']
                                                text = (ligne.text or '').strip()
                                                if text.startswith('Chapitre'):
                                                    chapitre = dict(
                                                        numero=element_to_inner_html(ligne),
                                                        )
                                                    chapitres.append(chapitre)
                                                    etat_ligne = 'titre_chapitre'
                                                    continue
                                                chapitre = dict(
                                                    titre=element_to_inner_html(ligne),
                                                    )
                                                chapitres.append(chapitre)
                                                etat_ligne = 'contenu_chapitre'
                                                continue
                                            if align == 'justify':
                                                if len(ligne) == 0 and not (ligne.text or '').strip():
                                                    continue
                                                if len(ligne) == 1 and not (ligne.text or '').strip():
                                                    element_ligne = ligne[0]
                                                    if element_ligne.tag == 'strong':
                                                        articles = []
                                                        titre['articles'] = articles
                                                        paragraphes = []
                                                        element_ligne.tag = 'span'
                                                        article = dict(
                                                            paragraphes=paragraphes,
                                                            titre=element_to_inner_html(element_ligne),
                                                            )
                                                        articles.append(article)
                                                        etat_ligne = 'article'
                                                        continue
                                    if etat_ligne == 'titre_chapitre':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align == 'center' and len(ligne) == 1:
                                                element_ligne = ligne[0]
                                                if element_ligne.tag == 'strong':
                                                    element_ligne.tag = 'span'
                                                    chapitre['titre'] = element_to_inner_html(element_ligne)
                                                    etat_ligne = 'contenu_chapitre'
                                                    continue
                                    if etat_ligne == 'contenu_chapitre':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align == 'center' and len(ligne) == 0 and not (ligne.text or '').strip():
                                                    continue
                                            if align == 'justify':
                                                if len(ligne) == 0 and not (ligne.text or '').strip():
                                                    continue
                                                if len(ligne) == 1 and not (ligne.text or '').strip():
                                                    element_ligne = ligne[0]
                                                    if element_ligne.tag == 'strong':
                                                        articles = []
                                                        chapitre['articles'] = articles
                                                        paragraphes = []
                                                        element_ligne.tag = 'span'
                                                        article = dict(
                                                            paragraphes=paragraphes,
                                                            titre=element_to_inner_html(element_ligne),
                                                            )
                                                        articles.append(article)
                                                        etat_ligne = 'article'
                                                        continue
                                    if etat_ligne == 'article':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align is None:
                                                if len(ligne) == 1 and not (ligne.text or '').strip():
                                                    element_ligne = ligne[0]
                                                    if element_ligne.tag == 'strong':
                                                        paragraphes = []
                                                        element_ligne.tag = 'span'
                                                        article = dict(
                                                            paragraphes=paragraphes,
                                                            titre=element_to_inner_html(element_ligne),
                                                            )
                                                        articles.append(article)
                                                        continue
                                            if align == 'center':
                                                text = (ligne.text or '').strip()
                                                if len(ligne) == 0 and not text:
                                                    continue
                                                if text.startswith('TITRE'):
                                                    ligne.tag = 'span'
                                                    del ligne.attrib['align']
                                                    titre = dict(
                                                        numero=element_to_inner_html(ligne),
                                                        )
                                                    titres.append(titre)
                                                    etat_ligne = 'titre_titre'
                                                    continue
                                                if text.startswith('Chapitre'):
                                                    ligne.tag = 'span'
                                                    del ligne.attrib['align']
                                                    chapitre = dict(
                                                        numero=element_to_inner_html(ligne),
                                                        )
                                                    chapitres.append(chapitre)
                                                    etat_ligne = 'titre_chapitre'
                                                    continue
                                                del ligne.attrib['align']
                                                signataires = [element_to_html(ligne)]
                                                etat_ligne = 'signataires'
                                                continue
                                            if align == 'justify':
                                                if len(ligne) == 0 and not (ligne.text or '').strip():
                                                    if paragraphes:
                                                        del ligne.attrib['align']
                                                        paragraphes.append(element_to_html(ligne))
                                                    continue
                                                if len(ligne) == 1 and not (ligne.text or '').strip():
                                                    element_ligne = ligne[0]
                                                    if element_ligne.tag == 'strong' and not (element_ligne.tail or '').strip():
                                                        paragraphes = []
                                                        element_ligne.tag = 'span'
                                                        article = dict(
                                                            paragraphes=paragraphes,
                                                            titre=element_to_inner_html(element_ligne),
                                                            )
                                                        articles.append(article)
                                                        continue
                                                del ligne.attrib['align']
                                                paragraphes.append(element_to_html(ligne))
                                                continue
                                        if ligne.tag == 'ul':
                                            paragraphes.append(element_to_html(ligne))
                                            continue
                                    if etat_ligne == 'signataires':
                                        if ligne.tag == 'p':
                                            align = ligne.get('align')
                                            if align == 'center':
                                                text = (ligne.text or '').strip()
                                                if len(ligne) == 0 and not text:
                                                    continue
                                                del ligne.attrib['align']
                                                signataires.append(element_to_html(ligne))
                                                continue
                                    raise Exception(
                                        "Élément inattendu dans l'état {}, sous-état {}, ligne {} : {}".format(
                                            etat,
                                            etat_enfant,
                                            etat_ligne,
                                            element_to_html(ligne),
                                            ),
                                        )
                                break
                        raise Exception("Élément inattendu dans l'état {}, sous-état {} : {}".format(
                            etat,
                            etat_enfant,
                            element_to_html(enfant),
                            ))
                    break
        raise Exception("Élément inattendu dans l'état {} : {}".format(
            etat,
            element_to_html(element),
            ))

    match = titre_re.match(titre_court_document)
    assert match is not None, titre_court_document

    document = dict(
        categorie=match.group('categorie').lower(),
        considerants=considerants,
        introduction=introduction,
        numero=match.group("numero"),
        organisme=organisme,
        sections=parties,
        signataires=signataires,
        titre=titre_document,
        titre_court=titre_court_document,
        )
    DocumentCompleter.complete_document(
        document,
        to_slug('{}-cnil-{}'.format(match.group('categorie'), match.group("numero"))),
        )

    if args.json:
        html_file_path_core, html_file_extension = os.path.splitext(args.html_file)
        with open(html_file_path_core + '.json', 'w', encoding='utf-8') as json_file:
            json.dump(document, json_file, ensure_ascii=False, indent=2, sort_keys=True)

    class MyMarkdownWriter(MarkdownWriter):
        document_template = document_markdown_template

        def write_document(self, document, dir):
            super().write_document(document, dir)
            document_dir = os.path.join(dir, document['slug'])
            self.write_data(
                document,
                dict(
                    considerants=self.paragraphs_to_markdown(document['considerants']),
                    ),
                os.path.join(document_dir, 'considerants.md'),
                self.new_template(considerants_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    introduction=self.paragraphs_to_markdown(document['introduction']),
                    ),
                os.path.join(document_dir, 'introduction.md'),
                self.new_template(introduction_markdown_template),
                )
            self.write_data(
                document,
                dict(
                    signataires=self.paragraphs_to_markdown(document['signataires']),
                    ),
                os.path.join(document_dir, 'signataires.md'),
                self.new_template(signataires_markdown_template),
                )
    markdown_writer= MyMarkdownWriter()
    markdown_writer.write_document(document, args.target_dir)

    return 0


if __name__ == '__main__':
    sys.exit(main())
